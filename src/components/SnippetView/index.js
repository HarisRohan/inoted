import React from 'react';
import {ScrollView, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {
  Menu,
  MenuProvider,
  MenuOptions,
  MenuOption,
  MenuTrigger,
} from 'react-native-popup-menu';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faListUl } from '@fortawesome/free-solid-svg-icons';

const SnippetView = ({id, notesList, navigation, deleteNotes}) => {
  return (
      <ScrollView>
        <TouchableOpacity
          style={styles.container}
          onPress={() => navigation.navigate('ViewNotes', {id: id})}>
          <View>
            <Text style={styles.noteTitle}>{notesList.noteTitle}</Text>
          </View>
          <MenuProvider style={styles.optionList}>
              <Menu style={{padding : 30}}>
                <MenuTrigger customStyles={triggerStyles}> 
                  <FontAwesomeIcon icon={faListUl} color={'white'} size={20}/>
                </MenuTrigger>
                <MenuOptions customStyles={optionsStyles}>
                  <MenuOption text='Edit' onSelect={() => navigation.navigate('EditNotes', {id: id})} />
                  <MenuOption text='Delete' onSelect={() => deleteNotes(id)} />
                  <MenuOption text='Cancel' onSelect={() => (null)} />
                </MenuOptions>
              </Menu>
            </MenuProvider>
        </TouchableOpacity>
      </ScrollView>
  );
};

export default SnippetView;


const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    paddingHorizontal: 20,
    backgroundColor: '#fdbc01',
    borderRadius: 15,
    marginHorizontal : 15,
    marginVertical : 10,
    alignItems : 'center',
    justifyContent : 'space-evenly',
    padding : 20,
  },
  noteTitle: {
    fontWeight: 'bold',
    fontSize: 20,
    color : 'white'
  },
  optionList: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
    paddingRight: 10,
  },
});

const triggerStyles = {
  
  triggerWrapper: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
  },
};

const optionsStyles = {
  optionWrapper: {
    backgroundColor: 'white',
  },
  optionText: {
    color: '#fdbc01',
  },
};
