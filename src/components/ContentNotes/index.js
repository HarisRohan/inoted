import React from 'react';
import {StyleSheet, Text, TextInput} from 'react-native';

const ContentNotes = ({
  label,
  placeholder,
  keyboardType,
  isTextArea,
  onChangeText,
  nameState,
  value,
}) => {
  if (isTextArea) {
    return (
      <>
        <Text style={styles.label}>{label}</Text>
        <TextInput
          multiline={true}
          numberOfLines={4}
          style={styles.notesArea}
          placeholder={placeholder}
          keyboardType={keyboardType}
          value={value}
          onChangeText={text => onChangeText (nameState, text)}
        />
      </>
    );
  }

  return (
    <>
      <Text style={styles.label}>{label}</Text>
      <TextInput
        style={styles.titleArea}
        placeholder={placeholder}
        keyboardType={keyboardType}
        value={value}
        onChangeText={text => onChangeText (nameState, text)}
      />
    </>
  );
};

export default ContentNotes;

const styles = StyleSheet.create({
  titleArea : {
    padding: 10,
    fontWeight : 'bold',
    fontSize : 30,
    color : '#fdbc01'
  },
  notesArea : {
    textAlignVertical: 'top',
    padding: 10,
    marginBottom: '20%',
    color : '#493200',
  },
  label: {
    fontSize: 16,
    marginBottom: 10,
  },
});