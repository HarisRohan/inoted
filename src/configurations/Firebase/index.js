import firebase from 'firebase'

firebase.initializeApp({
    apiKey: "AIzaSyAZddsZ9ptLJeHX-Bh2LCIzQTPeMhHWiUs",
    authDomain: "uas-mobile-programming-931e3.firebaseapp.com",
    projectId: "uas-mobile-programming-931e3",
    storageBucket: "uas-mobile-programming-931e3.appspot.com",
    messagingSenderId: "634502172431",
    appId: "1:634502172431:web:7f8f48c712bcb20c91f87a",
    measurementId: "G-TCQ8DXCPKH"    
})

const Firebase = firebase;

export default Firebase;