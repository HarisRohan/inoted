import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {Splash, Home, AddNotes, ViewNotes, EditNotes} from '../pages';

const Stack = createStackNavigator();

const Routes = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Splash"
        component={Splash}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Home"
        component={Home}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="AddNotes"
        component={AddNotes}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="ViewNotes"
        component={ViewNotes}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="EditNotes"
        component={EditNotes}
        options={{headerShown: false}}
      />
    </Stack.Navigator>
  );
};

export default Routes;
