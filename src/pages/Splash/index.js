import React, {useEffect} from 'react';
import {Image, View, StyleSheet} from 'react-native';
import Logo from '../../assets/splash.png';

const Splash = ({navigation}) => {
    useEffect(() => {
        setTimeout(() => {
            navigation.replace('Home');
        }, 2500);
    });
    return (
        <View style={styles.wrapper}>
            <Image source={Logo} style={styles.logo}/>
        </View>
    );
};

export default Splash;

const styles = StyleSheet.create({
    wrapper: {
        flex : 1,
        backgroundColor : '#493200',
        alignItems : 'center',
        justifyContent : 'center',
    },
    logo : {
        margin : 10,
        resizeMode : 'contain',
        width : '50%',
    },
});