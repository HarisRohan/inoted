import Splash from './Splash';
import Home from './Home';
import AddNotes from './AddNotes';
import ViewNotes from './ViewNotes'
import EditNotes from './EditNotes';

export {Splash, Home, AddNotes, ViewNotes, EditNotes};