import React, {Component} from 'react';
import {StyleSheet, Text, TouchableOpacity, View, Alert} from 'react-native';
import {ContentNotes} from '../../components';
import Firebase from '../../configurations/Firebase';

export default class EditNotes extends Component {
  constructor(props) {
    super(props);

    this.state = {
      noteTitle: '',
      note: '',
    };
  }

  componentDidMount() {
    Firebase.database()
    .ref("Notes/" + this.props.route.params.id)
    .once('value', (querySnapShot) => {
        let data = querySnapShot.val() ? querySnapShot.val() : {};
        let notesList = {...data};

        this.setState({
            noteTitle: notesList.noteTitle,
            note: notesList.note,
        });
    });
}

  onChangeText = (nameState, value) => {
    this.setState({
      [nameState]: value,
    });
  };

  onSubmit = () => {
    if (this.state.noteTitle && this.state.note) {
        const iNoted = Firebase.database().ref("Notes/" + this.props.route.params.id);
        const Notes = {
            noteTitle: this.state.noteTitle,
            note: this.state.note,
        }


        iNoted
            .update(Notes)
            .then((data) => {
                // this.props.navigation.replace('ViewNotes',  {id: id});
                this.props.navigation.replace('Home');

            })
            .catch((error) => {
                console.log("Error : ", error);
            })

    } else {
        Alert.alert("Ooops..", "Your notes can't be empty");
    }
  };

  render() {
    return (
      <View style={styles.container}>
        <ContentNotes
          placeholder="Note Title"
          onChangeText={this.onChangeText}
          value={this.state.noteTitle}
          nameState="noteTitle"
        />
        <ContentNotes
          placeholder="Type Your Note Here..."
          isTextArea={true}
          onChangeText={this.onChangeText}
          value={this.state.note}
          nameState="note"
        />

        <TouchableOpacity style={styles.textButton} onPress={() => this.onSubmit()}>
          <Text style={styles.texttextButton}>SAVE</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
    marginBottom: 50,
  },
  textButton: {
    backgroundColor: '#fdbc01',
    borderRadius: 60,
    padding: 10,
    paddingHorizontal : 35,
    shadowColor: 'rgb(201, 118, 0)',
    elevation: 10,
    flex: 1,
    position: 'absolute',
    margin: 30,
    top: 15,
    right: 0,
  },
  texttextButton: {
    fontSize : 20,
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
});
