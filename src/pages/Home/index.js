import React, {Component} from 'react';
import {
  Text,
  StyleSheet,
  View,
  TouchableOpacity,
  Alert,
  ScrollView,
  Image,
} from 'react-native';
import Blank from '../../assets/blank_note.png'
import Firebase from '../../configurations/Firebase';
import SnippetView from '../../components/SnippetView';

export default class Home extends Component {
  constructor(props) {
    super(props);

    this.state = {
      Notes: {},
      NotesKey: [],
    };
  }

  componentDidMount() {
    this.getContent();
  }

  deleteNotes = id => {
    Alert.alert('Delete', 'Are you sure you want to delete the note?', [
      {
        text: 'Cancel',
        style: 'Cancel',
      },
      {
        text: 'OK',
        onPress: () => {
          Firebase.database()
            .ref('Notes/' + id)
            .remove();
          this.getContent();
        },
      },
    ]);
  };

  getContent = () => {
    Firebase.database()
      .ref('Notes')
      .once('value', querySnapShot => {
        let data = querySnapShot.val() ? querySnapShot.val() : {};
        let notesList = {...data};

        this.setState({
          Notes: notesList,
          NotesKey: Object.keys(notesList),
        });
      });
  };

  render() {
    const {Notes, NotesKey} = this.state;
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Text style={styles.headerText}>NOTES</Text>
        </View>
        <View style={styles.notesMap}>
          <ScrollView>
              {NotesKey.length > 0 ? (
                NotesKey.map(key => (
                  <SnippetView
                    key={key}
                    notesList={Notes[key]}
                    id={key}
                    {...this.props}
                    deleteNotes={this.deleteNotes}
                  />
                ))
              ) : (
                <View style={styles.blankNote}>
                  <Image source={Blank} style={{resizeMode : 'contain', width : 250, marginTop : 100}}/>
                </View>
              )}
          </ScrollView>
        </View>
        <View style={styles.buttonWrapper}>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('AddNotes')}>
            <Text style={styles.buttonText}>Add Note</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#493200',
  },
  buttonWrapper: {
    backgroundColor: '#fdbc01',
    borderRadius: 60,
    padding: 15,
    paddingHorizontal :40,
    shadowColor: 'rgb(168, 102, 7)',
    elevation: 13,
    flex: 1,
    position: 'absolute',
    margin: 30,
    alignSelf: 'center',
    bottom: 0,
  },
  buttonText: {
    fontSize : 20,
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  header: {
    paddingLeft : 20,
    height: 100,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  headerText: {
    fontSize: 40,
    fontWeight: 'bold',
    color: '#fdbc01',
    textShadowColor: 'rgb(36, 25, 0)',
    textShadowOffset: {width: 0, height: 0},
    textShadowRadius: 10,
  },
  notesMap: {
    flex:1,
    borderTopRightRadius : 30,
    borderTopLeftRadius : 30,
    backgroundColor : '#f7f1cb',
    paddingTop : 50,
    paddingBottom : 100,
    padding : 10,
    shadowColor: 'rgb(168, 102, 7)',
    elevation: 10,
  },
  blankNote : {
    alignItems : 'center',
    justifyContent : 'center',
  }
});
