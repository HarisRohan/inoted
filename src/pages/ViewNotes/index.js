import React, { Component } from 'react';
import { Text, StyleSheet, View, ScrollView } from 'react-native';
import Firebase from '../../configurations/Firebase';

export default class ViewNotes extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
            Notes: {},
        };
    }

    componentDidMount() {
        Firebase.database()
        .ref("Notes/" + this.props.route.params.id)
        .once('value', (querySnapShot) => {
            let data = querySnapShot.val() ? querySnapShot.val() : {};
            let notesList = {...data};
  
            this.setState({
                Notes: notesList,
            });
        });
    }
    
    render() {
        const {Notes} = this.state;
        return (
            <View style={styles.container}>
                <Text style={styles.noteTitle}>{Notes.noteTitle}</Text>
                <ScrollView>
                    <Text style={styles.note}>{Notes.note}</Text>
                </ScrollView>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container : {
        padding : 20,
        paddingBottom : 10,
        marginBottom : 50,
    },
    noteTitle : {
        padding: 10,
        marginBottom: 20,
        fontWeight : 'bold',
        fontSize : 30,
        color : '#fdbc01',
        // borderWidth : 1,
        // borderColor : 'red',
    },
    note : {
        textAlignVertical: 'top',
        color : '#493200',
        padding: 10,
        marginBottom: 50,
        // borderWidth : 1,
        // borderColor : 'red',
    }
})
