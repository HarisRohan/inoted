import React, {Component} from 'react';
import {StyleSheet, Text, TouchableOpacity, View, Alert, ScrollView} from 'react-native';
import {ContentNotes} from '../../components';
import Firebase from '../../configurations/Firebase';

export default class AddNotes extends Component {
  constructor(props) {
    super(props);

    this.state = {
      noteTitle: '',
      note: '',
    };
  }

  onChangeText = (nameState, value) => {
    this.setState({
      [nameState]: value,
    });
  };

  onSubmit = () => {
    if (this.state.noteTitle && this.state.note) {
        const iNoted = Firebase.database().ref('Notes');
        const Notes = {
            noteTitle: this.state.noteTitle,
            note: this.state.note
        }
        
        iNoted
            .push(Notes)
            .then((data) => {
                this.props.navigation.replace('Home');
            })
            .catch((error) => {
                console.log("Error : ", error);
            })
    } else {
        Alert.alert("Ooops..", "There's nothing left to save");
    }
  };

  render() {
    return (
      <View style={styles.container}>
        <ScrollView>
          <ContentNotes
            placeholder="Note Title"
            onChangeText={this.onChangeText}
            value={this.state.noteTitle}
            nameState="noteTitle"
          />
          <ContentNotes
            placeholder="Type Your Note Here..."
            isTextArea={true}
            onChangeText={this.onChangeText}
            value={this.state.note}
            nameState="note"
          />
        </ScrollView>
        
        <TouchableOpacity style={styles.buttonWrapper} onPress={() => this.onSubmit()}>
          <Text style={styles.buttonText}>SAVE</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 30,
  },
  buttonWrapper: {
    backgroundColor: '#fdbc01',
    borderRadius: 60,
    padding: 15,
    paddingHorizontal :50,
    shadowColor: 'rgb(201, 118, 0)',
    elevation: 10,
    flex: 1,
    position: 'absolute',
    margin: 30,
    alignSelf: 'center',
    bottom: 0,
  },
  buttonText: {
    fontSize : 20,
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
});
